
# OpenLiberty
#FROM open-liberty:microProfile3-java11
#COPY ./target/jakartaHelloWorld.war /config/dropins/
###COPY openliberty/server.xml /config


# Wildfly
FROM jboss/wildfly:18.0.0.Final
COPY ./target/jakartaHelloWorld.war /opt/jboss/wildfly/standalone/deployments/

# Payara Not working. Need image with Java 11 support.
#FROM payara/server-full:5.193.1
#COPY ./target/jakartaHelloWorld.war $DEPLOY_DIR