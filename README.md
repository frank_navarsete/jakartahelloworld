# Fast way.
This will build and run the image. The port is random due to different applicationservers
Run `./buildAndRun.sh`  
Use `docker ps` to find which ports to visit

# Build
mvn clean package && docker build -t com.glitchcube/jakartahelloworld .

# RUN

docker rm -f jakartahelloworld || true && docker run -d -P --name jakartahelloworld com.glitchcube/jakartahelloworld 