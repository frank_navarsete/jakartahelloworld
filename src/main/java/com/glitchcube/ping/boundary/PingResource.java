package com.glitchcube.ping.boundary;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

import com.glitchcube.MyService;
import org.eclipse.microprofile.config.inject.ConfigProperty;

@Path("ping")
public class PingResource {

    @Inject
    @ConfigProperty(name = "message")
    String message;

    @Inject
    MyService service;

    @GET
    public String ping() {
        var text = "hello little world";
        return this.message + text + " Jakarta EE with MicroProfile 2+!" + service.fetchTime();
    }

}
