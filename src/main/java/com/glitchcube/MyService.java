package com.glitchcube;

import javax.ejb.Stateless;

@Stateless
public class MyService {

    public long fetchTime() {
        return System.currentTimeMillis();
    }
}
