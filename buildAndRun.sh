#!/bin/sh
mvn clean package && docker build -t com.glitchcube/jakartahelloworld .
docker rm -f jakartahelloworld || true && docker run -d -P --name jakartahelloworld com.glitchcube/jakartahelloworld
